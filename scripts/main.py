import pandas as pd
import logging
import sys

logger = logging.getLogger()


def main(filename):
    try:
        # Cargo en un pandas DataFrame el contenido del csv. Como no tiene las columnas en el csv, le paso el nombre.
        input = pd.read_csv('../files/{filename}'.format(filename=filename), header=None, names=['Household_ID',
                                                                                                 'Sequence_Num',
                                                                                                 'Program_Identifier'])

        # Creo un diccionario que acumulara la cantidad de veces que se vio cada programa por cada dispositivo
        tv_shows_counter = dict()

        # Obtengo una lista de (Household_ID, Sequence) unicos para utilizarlo a la hora de recorrer el input.
        id_sq_list = input[['Household_ID', 'Sequence_Num']] \
            .drop_duplicates(['Household_ID', 'Sequence_Num'], keep='first') \
            .sort_values(by=['Household_ID', 'Sequence_Num'])

        # Acumulador de "filas" para la salida
        output = []
        for id, sq in id_sq_list.to_records(index=False):
            # Lista los progamas pertenecientes a un (Household_ID, Sequence)
            prog_id_block = input[(input['Household_ID'] == id) & (input['Sequence_Num'] == sq)]['Program_Identifier']
            # Cuando el script corre la primera vez, no tiene nada contra que comparar, por lo tanto tiene que ir
            # llenando el diccionario acumulador

            # Le pido al diccionario que me devuelva el Household, si no lo encuentra que devuelva False
            if not tv_shows_counter.get(id, False):
                # Si no encuentra el Household, creo un diccionario que para cada programa le agrega un 1
                first_dict_tv = {tv_show: 1 for tv_show in prog_id_block.values}  # https://www.geeksforgeeks.org/python-dictionary-comprehension/
                tv_shows_counter[id] = first_dict_tv
                # Como aca todos los programas tienen la misma puntuacion (1) se elige uno al azar con el .sample(1)
                # A la lista de la salida le agrego la fila en forma de diccionario.
                output.append(
                    {'Household_ID': id, 'Sequence_Num': sq, 'Program_Identifier': prog_id_block.sample(1).item()})
            else:
                # En el caso de encontrar que el Household tiene informacion historica, me lo almaceno en un auxiliar
                dict_aux = tv_shows_counter[id]
                # Con el diccionario historico del Household en mano, recorro los programas del bloque y me fijo
                # si dicho programa ya esta presente. En le caso de estarlo actualizo el contador y sino lo agrego
                # con su contador en 1
                for programa in prog_id_block.values:
                    dict_aux[programa] = dict_aux.get(programa, 0) + 1
                # En este paso transformo el diccionario auxiliar en un DataFrame para hacer el merge con la lista
                # de programas actuales.
                df_historic_tvshows = pd.DataFrame(list(dict_aux.items()), columns=['Program_Identifier', 'Count'])
                # El merge (join) se hace para tener los contadores de solamente los programas del bloque actual
                # y se ordena de mayor a menor para tomar el primero o sea el mas frecuente con el [:1]
                selected_tv = \
                    df_historic_tvshows.merge(prog_id_block, how='right', on='Program_Identifier').sort_values(
                        by='Count',
                        ascending=False)['Program_Identifier'][:1]
                output.append(
                    {'Household_ID': id, 'Sequence_Num': sq, 'Program_Identifier': selected_tv.item()})
        # Una vez que tengo la lista de programas estimados, creo un dataframe para guardar el mismo en .csv.
        pd.DataFrame(output).to_csv('exercise_output.csv', index=False)
    except Exception as e:
        logger.error('%s - %s', 'main', str(e))


def logging_config(logger):
    formatter = logging.Formatter(fmt='%(asctime)s - %(module)s - %(levelname)s - %(message)s',
                                  datefmt='%Y-%m-%dT%H:%M:%S%z')
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    return logger


if __name__ == '__main__':
    logger = logging_config(logger)
    logger.info('=== INICIO ===')
    try:
        if len(sys.argv) > 1:
            filename = sys.argv[1]
            print(filename)
        else:
            logger.error('%s - %s', 'init', "No filename detected")

        main(filename)
        logger.info('=== FINAL ===')
    except Exception as e:
        logger.error('%s - %s', 'init', str(e))
